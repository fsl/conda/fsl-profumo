#!/usr/bin/env bash

export FSLDIR=${PREFIX}
export FSLDEVDIR=${PREFIX}

# install source
mkdir -p ${PREFIX}/src/
cp -r $(pwd) ${PREFIX}/src/${PKG_NAME}


# install data
mkdir -p  ${FSLDIR}/data/PROFUMO/HRFs/
cp -r ./HRFs/* ${FSLDIR}/data/PROFUMO/HRFs/

. ${FSLDIR}/etc/fslconf/fsl-devel.sh

make -C ./C++ clean
make -C ./C++
make -C ./C++ install

python -m pip install ./Python   \
   --prefix=${FSLDIR}            \
   --no-deps --ignore-installed  \
   --no-cache-dir -vvv
